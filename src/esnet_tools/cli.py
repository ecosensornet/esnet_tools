"""
Chapeau for all tools in ecosensornet
"""
from argparse import ArgumentParser

from serial.tools.list_ports import comports

from .copy_template import cp_tpl
from .upload_data import upload_data


def action_ls(**kwds):
    """List available serial ports
    """
    for port in comports():
        print("port", port.name)


def main():
    """Run CLI evaluation"""
    action = dict(
        ls=action_ls,
        tpl=cp_tpl,
        up=upload_data
    )
    # parse argument line
    parser = ArgumentParser(description="Ecosensornet tool manager")
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="increase output verbosity")

    subparsers = parser.add_subparsers(dest='subcmd', help='sub-command help')

    parser_ls = subparsers.add_parser('ls', help=action_ls.__doc__)

    parser_tpl = subparsers.add_parser('tpl', help=cp_tpl.__doc__)
    parser_tpl.add_argument("-d", "--dest", default=".",
                            help="Directory in which to copy files")
    parser_tpl.add_argument("-b", "--board", default="esp8266",
                            help="Board to use for templating")
    parser_tpl.add_argument("--local", dest='only_local', default=False, action='store_true',
                            help="Copy all or only local files")

    parser_up = subparsers.add_parser('up', help=upload_data.__doc__)
    parser_up.add_argument("-p", "--port", default="COM14",
                           help="Serial port to use to communicate with board")
    parser_up.add_argument("-n", "--node", default="none",
                           help="node id to fetch special config")

    kwds = vars(parser.parse_args())
    print("verbosity", kwds.pop('verbosity'))

    # perform action
    subcmd = kwds.pop('subcmd')
    action[subcmd](**kwds)
