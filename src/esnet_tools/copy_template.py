"""
Copy template node files into current dir
"""
import shutil
from pathlib import Path

import node_base

base = Path(node_base.__file__).parents[2]
assert base.exists()


def cp_tpl(dest='.', only_local=False, board='esp8266'):
    """Copy all files from node base

    Args:
        dest (str): Directory in which to copy files
        only_local (bool): Whether to copy all or only local files
        board (str): name of board

    Returns:
        None
    """
    if dest == '.':
        dest = Path.cwd()
    else:
        dest = Path(dest)
        assert dest.exists()

    root_dir = base / f"src/firmware/{board}"

    # ino file
    pth_ino = dest / f"{dest.name}.ino"
    if not pth_ino.exists():
        if only_local:
            code = """#include <LittleFS.h>

#include "src/calibration/calibration.h"
#include "src/measure/node_measure.h"

unsigned long t0;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
  delay(300);

  LittleFS.begin();
  calibration::setup();
  measure::setup();
}

void loop() {
  String msg = "";

  t0 = millis();
  measure::sample();
  measure::fmt_config(msg);

  Serial.println("elapsed: " + String(millis() - t0));
  Serial.println(msg);

  delay(1000);
}
"""
            pth_ino.write_text(code)
        else:
            pth_ino_ref = root_dir / f"{board}.ino"
            shutil.copy(pth_ino_ref, pth_ino)

    # all src files
    if only_local:
        shutil.copytree(root_dir / "src/calibration", dest / "src/calibration", dirs_exist_ok=True)
        shutil.copytree(root_dir / "src/measure", dest / "src/measure", dirs_exist_ok=True)
        shutil.copy(root_dir / "src/settings.h", dest / "src")
    else:
        shutil.copytree(root_dir / "src", dest / "src", dirs_exist_ok=True)
