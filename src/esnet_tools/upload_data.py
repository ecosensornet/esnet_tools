"""
Upload all data files on microcontrollers
"""
import os
import shutil
from pathlib import Path
from subprocess import run

try:
    board = os.environ['esp_board']
except KeyError:
    board = 'esp32'

if board == 'esp8266':
    board_pth = Path("C:/Users/jchopard/AppData/Local/Arduino15/packages/esp8266")
    assert board_pth.exists()

    tool_pth = board_pth / "tools/mklittlefs/3.0.4-gcc10.3-1757bed/mklittlefs.exe"
    assert tool_pth.exists()

    python_pth = board_pth / "tools/python3/3.7.2-post1/python3.exe"
    assert python_pth.exists()

    upload_script = board_pth / "hardware/esp8266/3.0.2/tools/upload.py"
    assert upload_script.exists()
else:
    board_pth = Path("C:/Users/jchopard/AppData/Local/Arduino15/packages/esp32")
    assert board_pth.exists()

    tool_pth = board_pth / "tools/mklittlefs/3.0.0-gnu12-dc7f933/mklittlefs.exe"
    assert tool_pth.exists()

    esptool_pth = board_pth / "tools/esptool_py/4.5.1/esptool.exe"
    assert esptool_pth.exists()


def _create_image(tmp_dir):
    data_pth = tmp_dir.absolute()
    print("data pth", data_pth)

    spi_page = 256
    spi_block = 4096
    if board == 'esp8266':
        spi_size = 64 * 1024
    else:
        spi_size = 1408 * 1024  # for esp32

    img_pth = Path("data_img.mklittlefs.bin").absolute()
    if img_pth.exists():
        print("rm", img_pth)
        img_pth.unlink()

    # line 306
    cmd = [tool_pth, "-d", str(5), "-c", data_pth, "-p", str(spi_page), "-b", str(spi_block), "-s", str(spi_size),
           img_pth]
    ret = run(cmd)

    print("mklittlefs", ret)

    return img_pth


def _upload_esp8266(img_pth, port):
    upload_speed = 115200
    upload_address = "0xEB000"

    # line 336
    cmd = [python_pth, upload_script,
           "--chip", "esp8266",
           "--port", port, "--baud", str(upload_speed),
           "write_flash", upload_address, img_pth]

    ret = run(cmd)
    print("upload", ret)


def _upload_esp32(img_pth, port):
    upload_speed = 921600
    upload_address = 2686976
    flash_mode = 'dio'
    flash_freq = '80m'

    cmd = [esptool_pth,
           "--chip", "esp32-S2",
           "--baud", str(upload_speed),
           "--port", port,
           "--before", "default_reset", "--after", "hard_reset",
           "write_flash", "-z", "--flash_mode", flash_mode, "--flash_freq", flash_freq, "--flash_size", "detect",
           str(upload_address), img_pth
           ]

    ret = run(cmd)
    print("upload", ret)


def upload_data(port, node=None):
    """upload all data files in ./src/**

    Args:
        port (str): Serial port

    Returns:
        None
    """
    # collect all data files in single temporary directory
    temp_dir = Path("data_tmp")
    if temp_dir.exists():
        for pth in temp_dir.glob("*.*"):
            pth.unlink()
    else:
        temp_dir.mkdir()

    assert len(list(temp_dir.glob("*.*"))) == 0

    for pth in Path("src").glob("**/*.*"):
        if pth.suffix in (".json", ".css", ".html"):
            print(pth)
            shutil.copy(pth, temp_dir)

    print("node", node)
    if node != "none":
        cfg_pth = Path(r"C:\Users\jchopard\Desktop\perso\ecosensornet\db") / node
        assert cfg_pth.exists()
        for pth in cfg_pth.glob("**/*.*"):
            if pth.suffix in (".json", ".css", ".html"):
                print(pth)
                shutil.copy(pth, temp_dir)  # overwrite previous file

    # create image
    img_pth = _create_image(temp_dir)

    # upload image
    if board == 'esp8266':
        _upload_esp8266(img_pth, port)
    else:
        _upload_esp32(img_pth, port)

    # cleanup
    img_pth.unlink()
    for pth in temp_dir.glob("*.*"):
        pth.unlink()

    temp_dir.rmdir()
