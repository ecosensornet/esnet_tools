========================
esnet_tools
========================

.. {# pkglts, doc

.. image:: https://ecosensornet.gitlab.io/esnet_tools/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/esnet_tools/

.. image:: https://ecosensornet.gitlab.io/esnet_tools/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/esnet_tools/0.0.1/

.. image:: https://ecosensornet.gitlab.io/esnet_tools/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/esnet_tools

.. image:: https://badge.fury.io/py/esnet_tools.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/esnet_tools

.. #}
.. {# pkglts, glabpkg, after doc

.. #}

Set of tools to manage packages in ecosensornet

